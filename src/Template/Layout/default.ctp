<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Ebay Auto Trading System by trictaft';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body id="<?= $this->request->action ?>">
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href="/">Ebay Auto Trading System</a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="">
            <?php 
                if ($this->request->session()->read('Auth.User.id')): 
            ?>
                <li><?= $this->Html->link(__('商品登録フォーム'), ['controller'=>'registers','action' => 'postfile',$this->request->session()->read('Auth.User.id')]) ?></li>
                <li><?= $this->Html->link(__('登録商品一覧'), ['controller'=>'items','action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('登録者成果一覧'), ['controller'=>'registers','action' => 'result']) ?></li>
                <li><?= $this->Html->link(__('登録者管理'), ['controller'=>'registers','action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('LOGOUT'), ['controller'=>'users','action' => 'logout']) ?></li>
            <?php endif; ?>
            </ul>
        </div>
    </nav>
    <div class="container clearfix">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>

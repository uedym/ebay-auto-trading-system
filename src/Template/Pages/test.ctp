<div id="top" class="index large-12 medium-12 columns content">
    <h1><img src="/img/logo03.png" alt=""></h1>
    <div class="wrap01">
        <?= $this->Form->create() ?>
        <fieldset>
            <?php
                echo $this->Form->label('user','USER');
                echo $this->Form->input('user',['label'=>false]);
                echo $this->Form->label('pass','PASSWORD');
                echo $this->Form->password('pass');
            ?>
        </fieldset>
        <?= $this->Form->button(__('LOGIN')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<div id="top" class="index large-12 medium-12 columns content">
    <h1><img src="/img/logo03.png" alt=""></h1>
    <div class="wrap01">
        <?= $this->Flash->render('auth') ?>
        <?= $this->Form->create() ?>
            <fieldset>
                <?= $this->Form->label('username','USER') ?>
                <?= $this->Form->input('username',['label'=>false]) ?>
                <?= $this->Form->label('password','PASSWORD') ?>
                <?= $this->Form->input('password',['label'=>false]) ?>
            </fieldset>
        <?= $this->Form->button(__('LOGIN')); ?>
        <?= $this->Form->end() ?>
    </div>
</div>
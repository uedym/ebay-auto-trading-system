<div class="items index large-12 medium-12 columns content">
    <h3><?= __('登録商品検索') ?></h3>
    <div id="searchForm" class="large-6 medium-6 columns">
        <?= $this->Form->create(null,['url' => ['controller' => 'Items','action' => 'search']]); ?>
        <fieldset>
            <?= $this->Form->label('find','キーワード検索'); ?>
            <?= $this->Form->input('find',["label"=>false]); ?>
            <?= $this->Form->button('検索') ?>
            <?= $this->Form->end() ?>
        </fieldset>
    </div>
    <!-- /#searchForm -->

    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col" style="width:140px;"><?= $this->Paginator->sort('ebayid',$title="ebay商品ID") ?></th>
                <th scope="col"><?= $this->Paginator->sort('title',$title="商品名") ?></th>
                <th scope="col"><?= $this->Paginator->sort('url',$title="情報元URL") ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('created',$title="登録日時") ?></th> -->
                <th scope="col" style="width:200px;"><?= $this->Paginator->sort('status',$title="状態") ?></th>
                <th scope="col" style="width:200px;"><?= h("登録者") ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('error',$title="エラー") ?></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($items as $item): ?>
            <tr>
                <!-- <td><?= h($item->id) ?></td> -->
                <td class="td_id">
                    <?php 
                        if(!empty($item->itemview)&&$item->status=="登録済み"||$item->status=="削除"):
                    ?>
                    <a href="<?= h($item->itemview) ?>" target="_blank" class="<?php if($item->status=="削除"){ echo "off"; } ?>"><?= h($item->ebayid) ?></a>
                    <?php if(!empty($item->thumbnail)): ?>
                        <img src="<?= h($item->thumbnail) ?>" alt="" class="<?php if($item->status=="削除"){ echo "off"; } ?>">
                    <?php endif; ?>
                <?php else: ?>
                    <?= h($item->ebayid) ?>
                <?php endif; ?>
                </td>
                <td><?= h($item->title) ?></td>
                <td><a href="<?= h($item->url) ?>" target="_blank"><?= h($item->url) ?></a></td>
                
                <!-- <td><?= h( date("Y/m/d H:i",strtotime($item->created."+9 hour")) ) ?></td> -->
                <td><?= h($item->status) ?></td>
                <td><?= h($registers[$item->register_id]) ?></td>
                <?php // debug($registers); ?>
                <!-- <td><?= h($item->error) ?></td> -->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<?php 
    $this->assign('title', "アイテム設定"); 
    // debug($this->request->params['pass']);
?>

<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Html->link(__('登録商品一覧'), ['controller'=>'items','action' => 'index']) ?></li>
    </ul>
</nav> -->
<div class="registers index large-12 medium-12 columns content">
    <h3><?= __('商品CSVファイルのアップロード') ?></h3>
    <div class="large-5 medium-5 columns">
    <fieldset>
    <?php 

        echo $this->Form->create(null,[
                'enctype' => 'multipart/form-data',
                'type' => 'post'
            ]);
        
        // $regIDs[0] = "選択してください";
        // foreach ($registers as $register) {
        //     $regIDs[$this->Number->format($register->regid)] = $this->Number->format($register->regid);
        // }
        // echo $this->Form->input('registerid',
        //   [   
        //   'label' => '登録者ID',
        //   'options'  => $regIDs,
        //   'type'   => 'select',
        //   'label'  => false
        // ]);

        echo $this->Form->label('csv','CSVファイル');
        echo $this->Form->file('csv');
        echo "<br>";
        echo "<br>";
        $options = [];
        foreach ($registers as $value) {
            $options[$value->id] = $value->name; 
        }
        echo $this->Form->label('register','登録者を選択');
        echo $this->Form->select('register', $options, ['empty' => "選択してください"]);
        echo $this->Form->hidden('owner',['value' => $this->request->params['pass'][0]]);
        echo $this->Form->button('登録する');
        echo $this->Form->end();

    ?>
    </fieldset>
    </div>
</div>

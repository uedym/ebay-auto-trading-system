<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('操作') ?></li>
        <li><?= $this->Form->postLink(
                __('登録者の削除'),
                ['action' => 'delete', $register->id],
                ['confirm' => __('この登録者を削除しますか？ # {0}?', $register->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('登録者一覧'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="registers form large-9 medium-8 columns content">
    <?= $this->Form->create($register) ?>
    <fieldset>
        <legend><?= __('登録者の編集') ?></legend>
        <?php
            echo $this->Form->input('name' ,['label'=>false]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('登録')) ?>
    <?= $this->Form->end() ?>
</div>

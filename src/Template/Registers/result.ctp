
<div class="registers index large-12 medium-12 columns content">
    <h3><?= __('登録者成果一覧') ?></h3>
    <div id="searchForm" class="large-6 medium-6 columns">
        <?= $this->Form->create(null,['url' => ['controller' => 'Registers','action' => 'result']]); ?>
        <fieldset>
            <?= $this->Form->label('period','期間を選択'); ?>
            <?php 
                $startPeriod = '2016-12';
                $endPeriod = date('Y-m');
                $options = [];

                $thePeriod = date('Y-m');
                $options = [$thePeriod=>date('Y年m月',strtotime($thePeriod))];
                $count = 1;
                while(strtotime($thePeriod)>strtotime($startPeriod)){
                    $thePeriod = date('Y-m',strtotime("-{$count} Month"));
                    $options += [$thePeriod=>date('Y年m月',strtotime($thePeriod))];
                    $count++;
                }

                // if($startPeriod!=$endPeriod){
                //     debug($thePeriod);
                // }else{
                //     $options = [date("Y-m")=>date("Y年m月")];
                    
                // }

                echo $this->Form->select('period', $options, ['default' => date('Y年m月')]);
            ?>
            <?= $this->Form->button('検索') ?>
            <?= $this->Form->end() ?>
        </fieldset>
    </div>
    <!-- /#searchForm -->
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id',$title="登録者ID") ?></th>
                <th scope="col"><?= $this->Paginator->sort('name',$title="名前") ?></th>
                <th scope="col"><?= h("成果") ?></th>
                <th scope="col"><?= "有効件数<br>（出品中+削除+取り消し待機）" ?></th>
                <!-- <th scope="col" class="actions"></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($registers as $register): ?>
            <tr>
                <td><?= $this->Number->format($register->id) ?></td>
                <td><?= h($register->name) ?></td>
                <!-- <td class="actions">
                    <?= $this->Html->link(__('編集'), ['action' => 'edit', $register->id]) ?>　　
                    <?= $this->Form->postLink(__('削除'), ['action' => 'delete', $register->id], ['confirm' => __('Are you sure you want to delete # {0}?', $register->id)]) ?>
                </td> -->
                <td>
                    <?php 
                        foreach ($countArray[$register->id] as $key => $count) {
                            echo "{$key}:{$count}<br>";
                        }
                    ?>
                </td>
                <td>
                    <?php 
                        echo $countArray[$register->id]["出品中"]+$countArray[$register->id]["削除"]+$countArray[$register->id]["取り消し待機"];
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

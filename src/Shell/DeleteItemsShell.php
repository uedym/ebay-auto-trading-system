<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\Utility\Xml;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use League\Csv\Reader;
use \Exception;

class DeleteItemsShell extends Shell
{
    public function initialize()
    {
        $this->url = 'https://api.sandbox.ebay.com/ws/api.dll';
        // $this->oath = 'AgAAAA**AQAAAA**aAAAAA**Za1MWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GiCZOBpQidj6x9nY+seQ**/d0DAA**AAMAAA**JY2MzmjXLAfSklzlhYikBmD0cMy0AgXXGrGYu0k5mwV7o00TH353+fD1OQzsqSvLbud20SsHvUjAaPNGeSTOAERc5dVpspE36Sg0agbtzshGY4q7v92J8pZUkiPbaFTyuobGAanZ6xQdAzHYf3k2VL9RWakB+YLu8FBhVbGaZm4FnoM2HbQrbQBe2UqhmoFbFD2wnxjMyzgwtkXzKmRymtIF1/IEhZ8D4w/Z2rH8kJHX7EYobW+BSCD+DTpIHHXRhBAAtt/snDnYMwqcl6VmrVugEv/xT/cL7+Zkv2f3Hx+abmUbH6UbofLA5FPKweRCPL47z6BfZ302Fb+ek78FbK2U+5HjZ/b9eX43p9/lD4DbYldG/gu6tx27/3H6/Dnyca74hnhsmjH1g0N8cYPt5Nq1buZUfFfM3lkbn+oN6QwxscBTPv186DTjV//EURYmsZjFPpWNj1DBAHJr6IKg2YDCmKJZphG1zE4l4MJv/IcqLg2Ryfcdxzkj5Oj+iL6I69Xj+YGlhVjYLHuYMXOkNd5n0nMQ1rmMR0/lkLsWXzhxi+j7O7goXZ4+fPKforWizN9KIKsaTwZ8ULu2pUoy81T5/9G/IUdWXI98s0Rb06RXNpRChjkNjZGr5x45UEy1Ndy3rZuYb2OYKQMkq+ELowXXr+nvezo63oHTrObYUkZvp3hrErw+I1UVqfQ4dvmMTwm9qcnRh9WaF9IDu6qdZuQXE4urHXOf0VzBTiLHbFwPKwtRL8xRU+bslopFqPWB';
        // $this->clientID = 'hiroshiu-ebayauto-SBX-399e85f71-5e9a526b';
        $this->url = 'https://api.ebay.com/ws/api.dll';
        $this->oath = 'AgAAAA**AQAAAA**aAAAAA**sQBaWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AGmYGpDpCKpw6dj6x9nY+seQ**32cDAA**AAMAAA**mM0OtECKbdFfq66xrOZdtdm4TORQ+cWpT3dXyuN4yFFm1pBcLtPJiTzcf4iXRLoaaRCWDmvLmZXkALi52iTkD5bcmMG+LxCub7+XsvYUI1ROwpJgEswNvV+/c3Kktnz6Uj0QZYkOJrJru0jrMPr6tIpG6rJq7NP34eFoO18YO7z4a1WCqdM3sRMWzdilDx04RGvk4L67i5EHjYpJf2Pz0BZ+WDL0sMPbzp/tOx1kVgp0uZX6lofcGUz5lpgu/6Axvz3UMNiHooiZmXKSL4WefQ/9w+oYY7uHG7MX+l8Xk/ZR7GeJOXk8DvubhE83u8xQdPRc/WehN8MqGqRLQ8PiH9cAq3gG3IxVtYuQ8UbdBmAXwEcdyFwBcrUzVBPfXjtGoWD1j1nIUidn5j75k6NJqN5kzV7MnL8vZZH56Y9VcJdYDsvKvdEiAPfs24doYN/K617f9AguwmKIzKX6DPcjJSDdNv2vV+lNQtNF9TXB+54I5Rkzjlv+pyI+bWv7YoaQAkMstQDCGaLMjbWPmNMAhoP8IsTnvskZ4aqkBXyPNf9nLejfc+scaq4DRloyO6l71xHzx/OhFTPEx6LxeYOeYBj/K20fCAm9B5D+i/lerS4dnP3VdZ53BEZcrNgmWxRH7VtBbtV/4JPX57dtMyb8Yxt+msgXHe2i0lAbBD533L42kWKI4OkGFBvGSCXrUNmiAM9ahkR6Qe6vyny6Or3kn2Hf+CjI4MyAaxaNbrIQRxPi+m1znq8ZTwMGTshor+0P';
        $this->clientID = 'hiroshiu-ebayauto-PRD-09a6113c8-5995592a';
    }

    public function main(){
    	$itemsTable = TableRegistry::get('Items');
		$item = $itemsTable->newEntity();

		$query = $itemsTable
			    ->find()
			    ->select(['id','ebayid', 'status'])
			    ->where(['status =' => '取り消し待機']);

		$results = $query->all();
		$data = $results->toArray();

		// debug($data);
		// return false;

		foreach ($data as $value) {
			$text = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";

			$text = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
					$text .= "<EndItemRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">";
					  $text .= "<RequesterCredentials>";
					    $text .= "<eBayAuthToken>{$this->oath}</eBayAuthToken>";
					  $text .= "</RequesterCredentials>";
					  $text .= "<ItemID>{$value->ebayid}</ItemID>";
					  $text .= "<EndingReason>NotAvailable</EndingReason>";
					$text .= "</EndItemRequest>";

			$http_headers = array(
				"Content-Type: text/xml",
				"X-EBAY-API-COMPATIBILITY-LEVEL: 967",
				"X-EBAY-API-CALL-NAME: EndItem",
				"X-EBAY-API-SITEID: 0",
		    	"X-EBAY-API-DEV-NAME: {$this->clientID}",
		    	"X-EBAY-API-APP-NAME: {$this->clientID}",
				"X-EBAY-API-CERT-NAME: {$this->clientID}"
			);
			$xml = $text;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
			 
			$result = curl_exec($ch);
			$return = Xml::toArray(Xml::build($result));

			if($return["EndItemResponse"]["Ack"]=="Success"||$return["EndItemResponse"]["Errors"]['LongMessage']=="The auction has already been closed."){
				$item = $itemsTable->get($value->id); 
				$item->status = '削除';
				$itemsTable->save($item);
				echo "id".$value->id."を削除しました。\n";
			}
		}
    }


}
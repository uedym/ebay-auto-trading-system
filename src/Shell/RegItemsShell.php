<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\Utility\Xml;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use League\Csv\Reader;
use \Exception;

class RegItemsShell extends Shell
{
    public function initialize()
    {
        $this->uploaddir = '/var/www/uploads/';
        // $this->url = 'https://api.sandbox.ebay.com/ws/api.dll';
        // $this->oath = 'AgAAAA**AQAAAA**aAAAAA**Za1MWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GiCZOBpQidj6x9nY+seQ**/d0DAA**AAMAAA**JY2MzmjXLAfSklzlhYikBmD0cMy0AgXXGrGYu0k5mwV7o00TH353+fD1OQzsqSvLbud20SsHvUjAaPNGeSTOAERc5dVpspE36Sg0agbtzshGY4q7v92J8pZUkiPbaFTyuobGAanZ6xQdAzHYf3k2VL9RWakB+YLu8FBhVbGaZm4FnoM2HbQrbQBe2UqhmoFbFD2wnxjMyzgwtkXzKmRymtIF1/IEhZ8D4w/Z2rH8kJHX7EYobW+BSCD+DTpIHHXRhBAAtt/snDnYMwqcl6VmrVugEv/xT/cL7+Zkv2f3Hx+abmUbH6UbofLA5FPKweRCPL47z6BfZ302Fb+ek78FbK2U+5HjZ/b9eX43p9/lD4DbYldG/gu6tx27/3H6/Dnyca74hnhsmjH1g0N8cYPt5Nq1buZUfFfM3lkbn+oN6QwxscBTPv186DTjV//EURYmsZjFPpWNj1DBAHJr6IKg2YDCmKJZphG1zE4l4MJv/IcqLg2Ryfcdxzkj5Oj+iL6I69Xj+YGlhVjYLHuYMXOkNd5n0nMQ1rmMR0/lkLsWXzhxi+j7O7goXZ4+fPKforWizN9KIKsaTwZ8ULu2pUoy81T5/9G/IUdWXI98s0Rb06RXNpRChjkNjZGr5x45UEy1Ndy3rZuYb2OYKQMkq+ELowXXr+nvezo63oHTrObYUkZvp3hrErw+I1UVqfQ4dvmMTwm9qcnRh9WaF9IDu6qdZuQXE4urHXOf0VzBTiLHbFwPKwtRL8xRU+bslopFqPWB';
        $this->url = 'https://api.ebay.com/ws/api.dll';
        $this->oath = 'AgAAAA**AQAAAA**aAAAAA**sQBaWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AGmYGpDpCKpw6dj6x9nY+seQ**32cDAA**AAMAAA**mM0OtECKbdFfq66xrOZdtdm4TORQ+cWpT3dXyuN4yFFm1pBcLtPJiTzcf4iXRLoaaRCWDmvLmZXkALi52iTkD5bcmMG+LxCub7+XsvYUI1ROwpJgEswNvV+/c3Kktnz6Uj0QZYkOJrJru0jrMPr6tIpG6rJq7NP34eFoO18YO7z4a1WCqdM3sRMWzdilDx04RGvk4L67i5EHjYpJf2Pz0BZ+WDL0sMPbzp/tOx1kVgp0uZX6lofcGUz5lpgu/6Axvz3UMNiHooiZmXKSL4WefQ/9w+oYY7uHG7MX+l8Xk/ZR7GeJOXk8DvubhE83u8xQdPRc/WehN8MqGqRLQ8PiH9cAq3gG3IxVtYuQ8UbdBmAXwEcdyFwBcrUzVBPfXjtGoWD1j1nIUidn5j75k6NJqN5kzV7MnL8vZZH56Y9VcJdYDsvKvdEiAPfs24doYN/K617f9AguwmKIzKX6DPcjJSDdNv2vV+lNQtNF9TXB+54I5Rkzjlv+pyI+bWv7YoaQAkMstQDCGaLMjbWPmNMAhoP8IsTnvskZ4aqkBXyPNf9nLejfc+scaq4DRloyO6l71xHzx/OhFTPEx6LxeYOeYBj/K20fCAm9B5D+i/lerS4dnP3VdZ53BEZcrNgmWxRH7VtBbtV/4JPX57dtMyb8Yxt+msgXHe2i0lAbBD533L42kWKI4OkGFBvGSCXrUNmiAM9ahkR6Qe6vyny6Or3kn2Hf+CjI4MyAaxaNbrIQRxPi+m1znq8ZTwMGTshor+0P';
        $this->clientID = 'hiroshiu-ebayauto-PRD-09a6113c8-5995592a';
    }

    public function main(){
        // $email = new Email('default');
        // $email->to('送信先メールアドレス') //送信したいメールアドレスで書き直すこと 
        //       ->subject('タイトル')
        //       ->send('本文');
        
        // uploadディレクトリからcsvファイル一覧を取得
        $csvs = $this->get_csv_list();


        // ファイルがなければ終了
        if(empty($csvs)){
        	return false;
        }

        // CSVをXMLに変換
        $this->fetchData = array();
        $xmls = array();
        $this->registers = array();

        $filesData = TableRegistry::get('Files');
        $registersData = TableRegistry::get('Registers');

        foreach ($csvs as $csv) {
            if(!empty($csv)){
        		$query = $filesData->find('all')
        				->where(["file =" => $csv]);
        		$row = $query->first();

                $csv_reader = Reader::createFromPath($this->uploaddir.$csv);
                $this->fetchData = $csv_reader->fetchAll();
        		$xmls[] = $this->make_xml();
        		$this->registers[] = $row->register_id;

        		unlink($this->uploaddir.$csv);
            }
        }
        
        // XMLが生成されていればebayに登録
        if(!empty($xmls)){
	        $this->ebay_regist_item($xmls);
        }



        // URLとサムネイルを取得
        $items = TableRegistry::get('Items');
        $get_items = $this->get_item_urls();


        foreach ($this->registers as $value) {
        	$query = $registersData->find('all')
    				->where(["id =" => $value]);
    		$row = $query->first();
    		// メールを送信
	        $email = new Email('default');
	        $email->from(['tricraftmail@gmail.com'=>'EATSシステム'])
	              ->to('tricraftmail@gmail.com') //送信したいメールアドレスで書き直すこと 
	              ->subject('CSVがアップロードされました')
	              ->send("登録者：{$row->name}がCSVをアップロードしました。");
        }

        



        if(!empty($get_items)){
	        foreach ($get_items as $key => $value) {
		        if(array_key_exists("ItemID", $value)){
		        	$query = $items->find('all')
		    			->where(["ebayid =" => $value["ItemID"],'status = '=>'登録済み']);
		    		// $results = $query->all();
		    		$data = $query->toArray();
			    	if(!empty($data)){
			    		$target = $data[0]->id;
			    		debug($target);

			    		if(!empty($target)&&$target!=null){
				    		$item = $items->get($target);
				    		$item->itemview = $value["ListingDetails"]['ViewItemURL'];
				    		$item->thumbnail = $value["PictureDetails"]['GalleryURL'];
				    		$items->save($item);
			    		}
			    	}
		        }
	        }
        }
    }

    private function get_csv_list(){
        // $dir = $_SERVER["DOCUMENT_ROOT"].'/../../uploads/';
        $handle = opendir($this->uploaddir);
        $files = array();
        while (false !== ($fileName = readdir($handle))) {
             if(is_file($this->uploaddir.$fileName)){
                $files[] = $fileName;
             }
        }
        closedir($handle);
        return $files;
    }

    private function check_url($url){
    	$items = TableRegistry::get('Items');
    	$query = $items->find()
    			->where(["url" => $url,"status" => "登録済み"]);
    	$count = $query->count();
    	return $count;
    }

    private function check_title($title){
    	$items = TableRegistry::get('Items');
    	$query = $items->find()
    			->where(["title" => $title,"status" => "登録済み"]);
    	$count = $query->count();
    	return $count;
    }


    private function make_xml(){
    	$titles = array();
    	foreach ($this->fetchData[0] as $key => $value) {
    		$titles[$key] = $value;
    	}

    	$csvData = array();
    	$count=0;
    	foreach ($this->fetchData as $key => $csv) {
    		if($key>0){
	    		for($i=1;$i<count($csv);$i++){
	    			$csvData[$key][$titles[$i]] = $csv[$i];
	    		}
    		}
    	}

		$xml = array();
    	foreach ($csvData as $data) {
			
		    $text = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
			$text .= "<AddFixedPriceItemRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">\n";
				$text .= "<RequesterCredentials>\n";
					$text .= "<eBayAuthToken>{$this->oath}</eBayAuthToken>\n";
				$text .= "</RequesterCredentials>\n";
			$text .= "<ErrorLanguage>en_US</ErrorLanguage>\n";
	  		$text .= "<Item>\n";
	  			$text .= "<Title>{$data['*Title']}</Title>\n";
	  			$text .= "<Description><![CDATA[".$data['*Description']."]]></Description>\n";
	  			$text .= "<PrimaryCategory><CategoryID>{$data['*Category']}</CategoryID></PrimaryCategory>\n";
	  			$text .= "<StartPrice>{$data['*StartPrice']}</StartPrice>\n";
	  			$text .= "<AutoPay>0</AutoPay>\n";
	  			if(array_key_exists("BestOfferEnabled", $data)&&!empty($data['BestOfferEnabled'])){
	  				$text .= "<BestOfferDetails><BestOfferEnabled>{$data['BestOfferEnabled']}</BestOfferEnabled></BestOfferDetails>\n";
	  			}
	  			$text .= "<CategoryMappingAllowed>true</CategoryMappingAllowed>\n";
	  			$text .= "<ConditionID>{$data['*ConditionID']}</ConditionID>\n";
	  			$text .= "<Country>JP</Country>\n";
	  			$text .= "<Currency>USD</Currency>\n";
	  			$text .= "<DispatchTimeMax>{$data['DispatchTimeMax']}</DispatchTimeMax>\n";
	  			$text .= "<ItemSpecifics>\n";
	  				$text .= "<NameValueList>\n"; 
						$text .= "<Name>Brand</Name>\n";
						$text .= "<Value>{$data['*C:Brand']}</Value>\n";
					$text .= "</NameValueList>\n";
					$text .= "<NameValueList>\n"; 
				        $text .= "<Name>Model</Name>\n";
				        $text .= "<Value>{$data['C:Model']}</Value>";
			        $text .= "</NameValueList>\n";
					$text .= "<NameValueList>\n"; 
						$text .= "<Name>MPN</Name>\n";
						$text .= "<Value>{$data['*C:MPN']}</Value>\n";
					$text .= "</NameValueList>\n";
					$text .= "<NameValueList> \n";
						$text .= "<Name>Body Type</Name>\n";
						$text .= "<Value>{$data['C:Body Type']}</Value>\n";
					$text .= "</NameValueList>\n";
					$text .= "<NameValueList>\n"; 
						$text .= "<Name>String Configuration</Name>\n";
						$text .= "<Value>{$data['C:String Configuration']}</Value>\n";
					$text .= "</NameValueList>\n";
					$text .= "<NameValueList>\n"; 
						$text .= "<Name>Dexterity</Name>\n";
						$text .= "<Value>{$data['C:Dexterity']}</Value>\n";
					$text .= "</NameValueList>\n";
					$text .= "<NameValueList>\n"; 
						$text .= "<Name>Body Color</Name>\n";
						$text .= "<Value>{$data['C:Body Color']}</Value>\n";
					$text .= "</NameValueList>\n";
					$text .= "<NameValueList>\n"; 
						$text .= "<Name>Body Material</Name>\n";
						$text .= "<Value>{$data['C:Body Material']}</Value>\n";
					$text .= "</NameValueList>\n";
					$text .= "<NameValueList>\n"; 
						$text .= "<Name>Country/Region of Manufacture</Name>\n";
						$text .= "<Value>{$data['C:Country/Region of Manufacture']}</Value>\n";
					$text .= "</NameValueList>\n";
	  			$text .= "</ItemSpecifics>\n";
	  			if(array_key_exists("*Duration", $data)&&!empty($data['*Duration'])){
	  				if($data['*Duration']=="GTC"){
	  					$duration = "GTC";
	  				}else{
	  					$duration = "Days_{$data['*Duration']}";
	  				}
	  				$text .= "<ListingDuration>{$duration}</ListingDuration>\n";
	  			}
	  			$text .= "<ListingType>FixedPriceItem</ListingType>\n";
	  			if(array_key_exists("MinimumBestOfferPrice", $data)&&!empty($data['MinimumBestOfferPrice'])){
		  			$text .= "<ListingDetails>\n";
				        $text .= "<MinimumBestOfferPrice>{$data['MinimumBestOfferPrice']}</MinimumBestOfferPrice>\n";
				    $text .=  "</ListingDetails>\n";
	  			}
	  			$text .= "<Location>{$data['*Location']}</Location>\n";
	  			$text .= "<PaymentMethods>PayPal</PaymentMethods>\n";
	  			$text .= "<PayPalEmailAddress>{$data['PayPalEmailAddress']}</PayPalEmailAddress>\n";

	  			$text .= "<PictureDetails>\n";

	  				$pictures = explode("|",$data['PicURL']);
	  				foreach ($pictures as $picture) {
	  					if(!empty($picture)){
	  						$text .= "<PictureURL>{$picture}</PictureURL>\n";
	  					}
	  				}
	  			$text .= "</PictureDetails>\n";
	  			if(array_key_exists("PostalCode", $data)&&!empty($data['PostalCode'])){
	  				$text .= "<PostalCode>{$data['PostalCode']}</PostalCode>\n";
	  			}
	  			$text .= "<Quantity>{$data['*Quantity']}</Quantity>\n";
	  			$text .= "<ReturnPolicy>\n";
	  				$text .= "<ReturnsAcceptedOption>{$data['ReturnsAcceptedOption']}</ReturnsAcceptedOption>\n";
	  				$text .= "<ReturnsWithinOption>{$data['ReturnsWithinOption']}</ReturnsWithinOption>\n";
	  				$text .= "<ShippingCostPaidByOption>{$data['ShippingCostPaidByOption']}</ShippingCostPaidByOption>\n";
	  			$text .= "</ReturnPolicy>\n";
	  			$text .= "<SellerProfiles>\n";
	  				$text .= "<SellerPaymentProfile>\n";
						$text .= "<PaymentProfileName>{$data['PaymentProfileName']}</PaymentProfileName>\n";
					$text .= "</SellerPaymentProfile>\n";
					$text .= "<SellerReturnProfile>\n";
						$text .= "<ReturnProfileName>{$data['ReturnProfileName']}</ReturnProfileName>\n";
					$text .= "</SellerReturnProfile>\n";
					$text .= "<SellerShippingProfile>\n";
			        	$text .= "<ShippingProfileName>{$data['ShippingProfileName']}</ShippingProfileName>\n";
			        $text .= "</SellerShippingProfile>\n";
	  			$text .= "</SellerProfiles>\n";
	  			$text .= "<ShippingDetails>\n";
					$text .= "<ShippingType>{$data['ShippingType']}</ShippingType>\n";
					$text .= "<ShippingServiceOptions>\n";
						$text .= "<ShippingServicePriority>{$data['ShippingService-1:Priority']}</ShippingServicePriority>\n";
				        $text .= "<ShippingService>{$data['ShippingService-1:Option']}</ShippingService>\n";
						$text .= "<ShippingServiceCost>{$data['ShippingService-1:Cost']}</ShippingServiceCost>\n";
					$text .= "</ShippingServiceOptions>\n";
			    $text .= "</ShippingDetails>\n";
	  			$text .= "<Site>US</Site>\n";
	  		$text .= "</Item>\n";
	  		$text .= "</AddFixedPriceItemRequest>\n";


			try{
				if(array_key_exists('URL', $data)){
					$xml[$data['URL']] = Xml::build($text);
					if (!$xml){
				      throw new Exception();
				    }
				}
			}catch (Exception $ex) {
		    	debug("error");
			}
    	}
		return $xml;
    }

    private function ebay_regist_item($files){
    	$http_headers = array(
				"Content-Type: text/xml",
				"X-EBAY-API-COMPATIBILITY-LEVEL: 967",
				"X-EBAY-API-CALL-NAME: AddFixedPriceItem",
				"X-EBAY-API-SITEID: 0",
		    	"X-EBAY-API-DEV-NAME: hiroshiu-ebayauto-SBX-399e85f71-5e9a526b",
		    	"X-EBAY-API-APP-NAME: hiroshiu-ebayauto-SBX-399e85f71-5e9a526b",
				"X-EBAY-API-CERT-NAME: hiroshiu-ebayauto-SBX-399e85f71-5e9a526b"
			);


    	$fileCount = 0;
    	foreach ($files as $file) {
    		if(is_array($file)){
	    		foreach ($file as $itemUrl => $value) {
					$itemsTable = TableRegistry::get('Items');
					$item = $itemsTable->newEntity();
		    		if(!empty($itemUrl)&&$this->check_url($itemUrl)==0){

		    			$xml = $value->asXML();
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $this->url);
						curl_setopt($ch, CURLOPT_POST, TRUE);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
						 
						$result = curl_exec($ch);
						$return = Xml::toArray(Xml::build($result));

						if(!empty($return)&&array_key_exists("AddFixedPriceItemResponse", $return)){
							if(array_key_exists("ItemID", $return["AddFixedPriceItemResponse"])&&!empty($return["AddFixedPriceItemResponse"]["ItemID"])){
								$item->ebayid = $return["AddFixedPriceItemResponse"]["ItemID"];
								$item->status = "登録済み";
								$error = "";
							}else{
								$error = "";
								if(is_array($return["AddFixedPriceItemResponse"]["Errors"])){
									foreach ($return["AddFixedPriceItemResponse"]["Errors"] as $message) {
										$error .= "・".$message['LongMessage']."<br>";
									}
								}else{
									$error = $return["AddFixedPriceItemResponse"]["Errors"]['LongMessage'];
								}
								$item->status = "登録エラー";
							}
							$item->error = $error;

							$item->title = (String)$value->Item->Title;
							$item->url = $itemUrl;
							$item->register_id = $this->registers[$fileCount];

							if ($itemsTable->save($item)) {
							    $id = $item->id;
							    echo $id."\n";
							}
		    			}
					}else{
						$item->error = "URLかタイトルが重複しています。";
						$item->title = (String)$value->Item->Title;
						$item->url = $itemUrl;

						if ($itemsTable->save($item)) {
						    $id = $item->id;
						    echo $id."：URLかタイトルが不正です。\n";
						}
					}
	    		}
    		}
    	$fileCount++;
    	}

    }

    public function get_item_urls(){
    	// date_default_timezone_set('UTF');
    	$start = date("Y-m-d",strtotime("-1 day"));
    	$end = date("Y-m-d H:i:s");
    	$text = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<GetSellerListRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">
					  <RequesterCredentials>
					    <eBayAuthToken>{$this->oath}</eBayAuthToken>
					  </RequesterCredentials>
					  <StartTimeFrom>{$start}</StartTimeFrom> 
  					  <StartTimeTo>{$end}</StartTimeTo> 
					  <ErrorLanguage>en_US</ErrorLanguage>
					  <WarningLevel>High</WarningLevel>
					  <GranularityLevel>Coarse</GranularityLevel> 
					  <IncludeWatchCount>true</IncludeWatchCount> 
					  <Pagination> 
					    <EntriesPerPage>200</EntriesPerPage> 
					  </Pagination> 
					</GetSellerListRequest>";
		$http_headers = array(
				"Content-Type: text/xml",
				"X-EBAY-API-COMPATIBILITY-LEVEL: 967",
				"X-EBAY-API-CALL-NAME: GetSellerList",
				"X-EBAY-API-SITEID: 0",
		    	"X-EBAY-API-DEV-NAME: {$this->clientID}",
		    	"X-EBAY-API-APP-NAME: {$this->clientID}",
				"X-EBAY-API-CERT-NAME: {$this->clientID}"
			);

		$xml = $text;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		 
		$result = curl_exec($ch);
		$return = Xml::toArray(Xml::build($result));

		if(array_key_exists('ItemArray',$return['GetSellerListResponse'])){
			$return_items = $return['GetSellerListResponse']['ItemArray']['Item'];
		}else{
			$return_items = "";
		}
		return $return_items;

    }

}
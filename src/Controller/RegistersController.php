<?php
namespace App\Controller;

use App\Controller\AppController;
use League\Csv\Reader;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * Registers Controller
 *
 * @property \App\Model\Table\RegistersTable $Registers
 */
class RegistersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Registers->find('all')
                                 ->where(['user_id'=>$this->request->session()->read('Auth.User.id')]);
        $registers = $this->paginate($query);

        $this->set(compact('registers'));
        $this->set('_serialize', ['registers']);
    }

    /**
     * Result method
     *
     * @return \Cake\Network\Response|null
     */
    public function result()
    {
        $query = $this->Registers->find('all')
                                 ->where(['user_id'=>$this->request->session()->read('Auth.User.id')]);
        $registers = $query->all();

        $itemsTable = TableRegistry::get('Items');

        $statusArray = ['登録済み','削除','取り消し待機','登録エラー'];

        $countArray = [];

        if ($this->request->is('post')) {
            $startMonth = date("Y-m-01 00:00:00",strtotime($this->request->data['period']));
            $endMonth   = date("Y-m-01 00:00:00",strtotime($startMonth . "+1 month"));
        }else{
            $startMonth = date("Y-m-01 00:00:00");
            $endMonth   = date("Y-m-01 00:00:00",strtotime($startMonth . "+1 month"));
        }


        // $endMonth   = date("Y-m-d",strtotime($target_day . "+1 month"));

        foreach ($registers as $register) {
            foreach ($statusArray as $value) {
                $query = $itemsTable->find('all')
                                    ->where(['register_id'  => $register->id])
                                    ->where(['status'       => $value])
                                    ->where(['created >='   => $startMonth])
                                    ->where(['created <'    => $endMonth]);
                // $results = $query->all();
                if($value=="登録済み"){
                    $value = "出品中";
                }
                $countArray[$register->id][$value] = $query->count();
            }

        }

        $this->set(compact('registers','countArray'));
        $this->set('_serialize', ['registers']);
    }

    /**
     * View method
     *
     * @param string|null $id Register id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $register = $this->Registers->get($id, [
            'contain' => []
        ]);

        $this->set('register', $register);
        $this->set('_serialize', ['register']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $register = $this->Registers->newEntity();
        if ($this->request->is('post')) {
            $register = $this->Registers->patchEntity($register, $this->request->data);
            if ($this->Registers->save($register)) {
                $this->Flash->success(__('登録者を追加しました。'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('登録者を追加出来ませんでした。再度登録を試してください。'));
            }
        }
        $this->set(compact('register'));
        $this->set('_serialize', ['register']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Register id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $register = $this->Registers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $register = $this->Registers->patchEntity($register, $this->request->data);
            if ($this->Registers->save($register)) {
                $this->Flash->success(__('変更を保存しました。'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('変更を保存できませんでした。'));
            }
        }
        $this->set(compact('register'));
        $this->set('_serialize', ['register']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Register id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $register = $this->Registers->get($id);
        if ($this->Registers->delete($register)) {
            $this->Flash->success(__('登録者を削除しました。'));
        } else {
            $this->Flash->error(__('登録者を削除できませんでした。'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Postfile method
     *
     * @param string|null $id Register id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function postfile()
    {
        
        if($this->request->params['pass'][0]==null){
            return $this->redirect(
                ['controller' => 'Users', 'action' => 'login']
            );
        }

        $query = $this->Registers->find('all')
                                 ->where(['user_id'=>$this->request->params['pass'][0]]);
        $registers = $this->paginate($query);
        $this->set(compact('registers'));
        $this->set('_serialize', ['registers']);

        
        // POSTでエラーチェックがなければファイルをアプロードして処理
        if($this->request->is('post')&&$this->error_check()){
            $csv = $this->request->data('csv');
            $register = $this->request->data('register');
            if($this->csv_upload()){
                $filesTable = TableRegistry::get('Files');
                $files = $filesTable->newEntity();
                $files->file = $csv["name"];
                $files->register_id = $register;
                $filesTable->save($files);
            }
        }
    }

    private function error_check(){
        
        $csv = $this->request->data('csv');
        $register = $this->request->data('register');
        $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/../../uploads/';
        $uploadfile = $uploaddir . basename($csv['name']);
        // $regID = $this->request->data('registerid');
        $errorCheck = true;
        // if(empty($regID)){
        //     $this->Flash->error('登録者IDを選択してください。');
        //     $errorCheck = false;
        // }

        $filetype = explode(".",$csv['name']);
        $filetypeName = end($filetype);
        if(empty($csv)||$filetypeName!="csv"){
            $this->Flash->error('CSVファイルを選択してください。');
            $errorCheck = false;
        }elseif(file_exists($uploadfile)){
            $this->Flash->error('同じ名前のファイルが存在します。名前を変更して再登録してください。');
            $errorCheck = false;
        }
        if(empty($register)){
            $this->Flash->error('登録者を選択してください。');
            $errorCheck = false;
        }
        return $errorCheck;
    }

    private function csv_upload(){
        $csv = $this->request->data('csv');
        $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/../../uploads/';
        $uploadfile = $uploaddir . basename($csv['name']);

        if(!file_exists($uploaddir)){
            mkdir($uploaddir);
        }
        if (move_uploaded_file($csv['tmp_name'], $uploadfile)) {
            $this->Flash->set('アップロードを完了しました。');
            return true;
        } else {
            $this->Flash->error('アップロードに失敗しました。');
            return false;
        }

    }

    public function isAuthorized($user)
    {
        // All registered users can add articles
        if ($this->request->action === 'add') {
            return true;
        }

        // The owner of an article can edit and delete it
        if (in_array($this->request->action, ['edit', 'delete'])) {
            $registerId = (int)$this->request->params['pass'][0];
            if ($this->Registers->isOwnedBy($registerId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }


}

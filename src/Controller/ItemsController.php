<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;



/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 */
class ItemsController extends AppController
{
    public $paginate = [
        'limit' => 100,
        'order' => [
            'Items.id' => 'desc'
        ]
    ];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        
        $registersTable = TableRegistry::get('Registers');
        $query = $registersTable->find('all')
                                ->where(['user_id'=>$this->request->session()->read('Auth.User.id')]);
        $data = $query->toArray();
        $registers = array();
        foreach ($data as $value) {
           $registers[$value->id] = $value->name;
        }

        // if ($this->request->is('put')) {
        //     $find = $this->request->data['find'];
        //     $items = $this->paginate($this->Items->find()
        //                     ->orWhere(["title like " => '%' . $find . '%'])
        //                     ->orWhere(["ebayid like " => '%' . $find . '%'])
        //                     ->orWhere(["status like " => '%' . $find . '%'])
        //                     ->orWhere(["url like " => '%' . $find . '%']));
        //  }else{
        // }  

        $items = $this->paginate($this->Items);

       
        $this->set(compact('items','registers'));
        $this->set('_serialize', ['items'],['registers']);

    }

    public function search()
    {
        
        $registersTable = TableRegistry::get('Registers');
        $query = $registersTable->find('all')
                                ->where(['user_id'=>$this->request->session()->read('Auth.User.id')]);
        $data = $query->toArray();
        $registers = array();
        foreach ($data as $value) {
           $registers[$value->id] = $value->name;
        }

        if ($this->request->is('post')) {
            $find = $this->request->data['find'];
            if($find == "出品中"){
                $find = "登録済み";
            }

            // $keyword = mb_convert_kana($find, 's');
            // $ary_keyword = preg_split('/[\s]+/', $keyword, -1, PREG_SPLIT_NO_EMPTY);

            // foreach( $ary_keyword as $val ){
                
            // }

            $items = $this->paginate($this->Items->find()
                            ->orWhere(["title like " => '%' . $find . '%'])
                            ->orWhere(["ebayid like " => '%' . $find . '%'])
                            ->orWhere(["status like " => '%' . $find . '%'])
                            ->orWhere(["url like " => '%' . $find . '%']));
         }


       
        $this->set(compact('items','registers'));
        $this->set('_serialize', ['items'],['registers']);

    }

    /**
     * View method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => []
        ]);

        $this->set('item', $item);
        $this->set('_serialize', ['item']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $item = $this->Items->newEntity();
        if ($this->request->is('post')) {
            $item = $this->Items->patchEntity($item, $this->request->data);
            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('item'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $item = $this->Items->patchEntity($item, $this->request->data);
            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('item'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $item = $this->Items->get($id);
        if ($this->Items->delete($item)) {
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function cancel()
    {

        if ($this->request->is('post')) {
            $id = $this->request->data['item_id'];
            $query = $this->Items->query();
            $query->update()
                ->set(['status' => "取り消し待機"])
                ->where(['id' => $id])
                ->execute();
            $this->Flash->success(__('アイテムを取り消し待機にしました。'));

            return $this->redirect(['action' => 'index']);
            return false;
        }


        if($this->request->params['pass'][0]==null){
            return $this->redirect(
                ['controller' => 'Items', 'action' => 'index']
            );
        }

        $item = $this->Items->get($this->request->params['pass'][0]);
        if($item->status!="登録済み"){
            return $this->redirect(
                ['controller' => 'Items', 'action' => 'index']
            );
        }


        $registersTable = TableRegistry::get('Registers');
        $query = $registersTable->find('all')
                                ->where(['user_id'=>$this->request->session()->read('Auth.User.id')]);
        $data = $query->toArray();


        $registers = array();
        foreach ($data as $value) {
           $registers[$value->id] = $value->name;
        }
        // debug($item);
        // $item->status = "取り消し予約";

        // if($items->save($item)){

        // }

        // $item = $this->Items->newEntity();
        // if ($this->request->is('post')) {
        //     $item = $this->Items->patchEntity($item, $this->request->data);
        //     if ($this->Items->save($item)) {
        //         $this->Flash->success(__('The item has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     } else {
        //         $this->Flash->error(__('The item could not be saved. Please, try again.'));
        //     }
        // }
        $this->set(compact('item','registers'));
        $this->set('_serialize', ['item'], ['registers']);
    }


    public function urls() {
        // $items = $this->paginate($this->Items);

        $query = $this->Items->find('all', [
            'conditions' => ['Items.status =' => "登録済み"],
            'contain' => []
        ]);

       $data = $query->toArray();



        $this->set(compact('data'));
        $this->set('_serialize', ['data']);

    }

    public function stop(){
        $id = $this->request->query('id');
        $query = $this->Items->query();
        $query->update()
            ->set(['status' => "取り消し待機"])
            ->where(['id' => $id])
            ->execute();

        $this->set(compact('id'));
        $this->set('_serialize', ['id']);
    }

    public function hurryup()
    {
        $cmd = "sh /var/www/vhosts/sub0000544394.hmk-temp.com/regitems.sh";
        $retutn = exec($cmd,$return);
        $this->set(compact('return'));
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['urls']);
    }


}

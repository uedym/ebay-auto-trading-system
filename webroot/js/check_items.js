let client = require('cheerio-httpcli');
let http = require('http');
const URL = 'http://sub0000544467.hmk-temp.com/items/urls.json';


client.timeout = 180000;

http.get(URL, (res) => {
  let body = '';
  res.setEncoding('utf8');

  res.on('data', (chunk) => {
      body += chunk;
  });

  res.on('end', (res) => {
	res = JSON.parse(body);
	var sequence = Promise.resolve();
	var ids = new Array;
	res.data.forEach(function (currVal) {
		sequence = sequence
		.then(function () {
			getSites(currVal);
		})
	});
  });
}).on('error', (e) => {
  console.log(e.message); //エラー時
});



function getSites(val) {
    return new Promise((resolve, reject) => {
    var id = val.id;
    var url = val.url;
    var p = client.fetch(url);
    	p.then(function (result) {
			if(!chkSite(url,result)){
				// エラーなら出品解除
		  		stopItem(id);
			}
		})
		
		p.catch(function (err) {
		  // エラーなら出品解除
		  stopItem(id);
		});
  })
}

function stopItem(item){
	var url = 'http://sub0000544467.hmk-temp.com/items/stop.json?id='+item;
	http.get(url, function(res){
		var body = '';
		res.setEncoding('utf8');

		res.on('data', function(chunk){
			body += chunk;
		});

		res.on('end', function(res){
			ret = JSON.parse(body);
			console.log('id'+ret.id+'を削除予約しました。');
		});
	}).on('error', function(e){
		console.log(e.message); //エラー時
	});
}

function chkSite(url,result){
	domain = separateSite(url);
	var ret = true;
	switch (domain) {
		case 'miki':
			ret = chkMiki(result);
		break;
		case 'yahoo':
			ret = chkYahoo(result);
		break;
		case 'jguitar':
			ret = chkJguitar(result);
		break;
		case 'digim':
			ret = chkDigim(result);
		break;
		case 'soundh':
			ret = chkSoundh(result);
		break;
		case 'amazon':
			ret = chkAmazon(result);
		break;
		default:
			ret = true;
		break;
	}
	return ret;
}

// サイトをドメインで分割
function separateSite(url){
	var domains = {
			miki 	: 'mikigakki.com',
			yahoo 	: 'yahoo.co.jp',
			jguitar : 'j-guitar.com',
			digim 	: 'digimart.net',
			soundh 	: 'soundhouse.co.jp',
			amazon 	: 'amazon.co.jp'
		};
	var ret = '';

	if(url.indexOf(domains.miki) != -1){
		ret = 'miki';
	}
	if(url.indexOf(domains.yahoo) != -1){
		ret = 'yahoo';
	}
	if(url.indexOf(domains.jguitar) != -1){
		ret = 'jguitar';
	}
	if(url.indexOf(domains.digim) != -1){
		ret = 'digim';
	}
	if(url.indexOf(domains.soundh) != -1){
		ret = 'soundh';
	}
	if(url.indexOf(domains.amazon) != -1){
		ret = 'amazon';
	}

	return ret;
}

// 三木楽器チェック
function chkMiki(result){
	var ret = true;
	// タイトルに入力エラーが含まれたらエラー
	var title = result.$('title').text();
	if(title.indexOf('入力エラー') != -1){
		ret = false;
	}
	// ステータスがSOLD OUTならエラー
	var status = result.$('.CartOther > img').attr('alt');
	if(status == 'SOLD OUT'||status == 'HOLD'){
		ret = false;
	}

	return ret;
}

// ヤフオクチェック
function chkYahoo(result){
	var ret = true;
	// ステータスがこのオークションは終了していますならエラー
	var status = result.$('#modCloseBtn > p > img').attr('alt');
	if(status == 'このオークションは終了しています'){
		ret = false;
	}
	
	return ret;
}

// J-Guitarsチェック
function chkJguitar(result){
	var ret = true;

	// ステータスが販売済(SOLD)ならエラー
	var status = result.$('#upper > .cnts > .sub_ttl > h2 > img').attr('alt');
	if(status == '販売済(SOLD)' || status == '商談中(HOLD)'){
		ret = false;
	}

	return ret;
}

// デジマートチェック
function chkDigim(result){
	var ret = true;

	// ステータスが販売済(SOLD)ならエラー
	var status = result.$('.itemState  > .order > img').attr('alt');
	if(status == '販売済(SOLD)'){
		ret = false;
	}

	return ret;
}

// サウンドハウスチェック
function chkSoundh(result){
	var ret = true;

	// タイトルに商品が見つかりませんが含まれたらエラー
	var title = result.$('title').text();
	if(title.indexOf('商品が見つかりません') != -1){
		ret = false;
	}

	return ret;
}

// アマゾンチェック
function chkAmazon(result){
	var ret = true;

	// タイトルに商品が見つかりませんが含まれたらエラー
	// var title = result.$('title').text();
	// if(title.indexOf('商品が見つかりません') != -1){
	// 	ret = false;
	// }

	return ret;
}
